/* For information on usage and redistribution, and for a DISCLAIMER OF ALL
* WARRANTIES, see the file, "LICENSE.txt," in this distribution.

iem_dp written by IOhannes m zmoelnig, Thomas Musil, Copyright (c) IEM KUG Graz Austria 1999 - 2007 */
/* double precision library */

#include "m_pd.h"
#include "iemlib.h"
#include "iem_dp.h"


/* ------------------------  min_dp  ---------------------------- */
/* based on miller's min, which is part of pd, only with double precision */

static t_class *min_dp_class;

typedef struct _min_dp
{
  t_object  x_obj;
  t_float   x_coarse_left;
  t_float   x_fine_left;
  t_float   x_coarse_right;
  t_float   x_fine_right;
  t_outlet  *x_out_coarse;
  t_outlet  *x_out_fine;
} t_min_dp;

static void min_dp_bang(t_min_dp *x)
{
  double dleft, dright;

  dleft = iem_dp_calc_sum(x->x_coarse_left, x->x_fine_left);
  dright = iem_dp_calc_sum(x->x_coarse_right, x->x_fine_right);
  if(dleft < dright)
  {
    outlet_float(x->x_out_fine, x->x_fine_left);
    outlet_float(x->x_out_coarse, x->x_coarse_left);
  }
  else
  {
    outlet_float(x->x_out_fine, x->x_fine_right);
    outlet_float(x->x_out_coarse, x->x_coarse_right);
  }
}

static void min_dp_float(t_min_dp *x, t_floatarg f)
{
  x->x_coarse_left = f;
  min_dp_bang(x);
}

static void *min_dp_new(t_symbol *s, int ac, t_atom *av)
{
  t_min_dp *x = (t_min_dp *)pd_new(min_dp_class);

  floatinlet_new(&x->x_obj, &x->x_fine_left);
  floatinlet_new(&x->x_obj, &x->x_coarse_right);
  floatinlet_new(&x->x_obj, &x->x_fine_right);
  x->x_coarse_left = 0.0f;
  x->x_fine_left = 0.0f;
  if((ac > 0) && (IS_A_FLOAT(av, 0)))
    x->x_coarse_right = atom_getfloatarg(0, ac, av);
  else
    x->x_coarse_right = 0.0f;
  if((ac > 1) && (IS_A_FLOAT(av, 1)))
    x->x_fine_right = atom_getfloatarg(1, ac, av);
  else
    x->x_fine_right = 0.0f;
  x->x_out_coarse = outlet_new(&x->x_obj, &s_float);
  x->x_out_fine = outlet_new(&x->x_obj, &s_float);
  return (x);
}

void min_dp_setup(void)
{
  min_dp_class = class_new(gensym("min__"), (t_newmethod)min_dp_new, 0, sizeof(t_min_dp), 0, A_GIMME, 0);
  class_addcreator((t_newmethod)min_dp_new, gensym("min''"), A_GIMME, 0);
  class_addbang(min_dp_class, min_dp_bang);
  class_addfloat(min_dp_class, min_dp_float);
}
