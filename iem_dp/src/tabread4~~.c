/* For information on usage and redistribution, and for a DISCLAIMER OF ALL
* WARRANTIES, see the file, "LICENSE.txt," in this distribution.

iem_dp written by IOhannes m zmoelnig, Thomas Musil, Copyright (c) IEM KUG Graz Austria 1999 - 2007 */
/* double precision library */


#include "m_pd.h"
#include "iemlib.h"
#include "iem_dp.h"

/* -------------------------- tabread4~~ ------------------------------ */
/* based on miller's tabread4~ which is part of pd */

static t_class *tabread4_tilde_tilde_class;

typedef struct _tabread4_tilde_tilde
{
  t_object  x_obj;
  int       x_npoints;
  iemarray_t  *x_vec;
  t_symbol  *x_arrayname;
  t_float   x_f;
} t_tabread4_tilde_tilde;

static void *tabread4_tilde_tilde_new(t_symbol *s)
{
  t_tabread4_tilde_tilde *x = (t_tabread4_tilde_tilde *)pd_new(tabread4_tilde_tilde_class);
  x->x_arrayname = s;
  x->x_vec = 0;
  x->x_npoints=0;

  inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);
  outlet_new(&x->x_obj, &s_signal);
  x->x_f = 0;
  return (x);
}

static t_int *tabread4_tilde_tilde_perform(t_int *w)
{
  t_tabread4_tilde_tilde *x = (t_tabread4_tilde_tilde *)(w[1]);
  t_sample *in0 = (t_sample *)(w[2]);
  t_sample *in1 = (t_sample *)(w[3]);
  t_sample *out = (t_sample *)(w[4]);
  int n = (int)(w[5]);
  int maxindex;
  iemarray_t *buf = x->x_vec, *wp;
  int i;

  maxindex = x->x_npoints - 3;
  if (!buf){
    while (n--) *out++ = 0;
    return (w+6);
  }
  //  post("xxx");
  for (i = 0; i < n; i++)
  {
    t_sample in0_s=*in0++;
    t_sample in1_s=*in1++;
        double findex = iem_dp_calc_sum(in0_s, in1_s);
    long int index = findex;
    double frac;
    t_sample a,  b,  c,  d, cminusb;

    if (index < 1)
      index = 1, frac = 0;
    else if (index > maxindex)
      index = maxindex, frac = 1;
    else frac = findex - index;

    //  post("%f + %f = %f", in0_s, in1_s, findex);
    //  post("%f - %f = %d", findex, frac, index);

    wp = buf + index;

    a = iemarray_getfloat(wp,-1);
    b = iemarray_getfloat(wp, 0);
    c = iemarray_getfloat(wp, 1);
    d = iemarray_getfloat(wp, 2);

    cminusb = c-b;
    *out++ = b + frac * (cminusb - 0.1666667f * (1.-frac) *
                         ((d - a - 3.0f * cminusb) * frac + (d + 2.0f*a - 3.0f*b)));
  }
  return (w+6);
}

static void tabread4_tilde_tilde_set(t_tabread4_tilde_tilde *x, t_symbol *s)
{
  t_garray *a;

  x->x_arrayname = s;
  if (!(a = (t_garray *)pd_findbyclass(x->x_arrayname, garray_class)))
  {
    if (*s->s_name)
      pd_error(x, "tabread4~~: %s: no such array", x->x_arrayname->s_name);
    iemarray_setfloat(x->x_vec, 0, 0.0f);
  }
  else if (!iemarray_getarray(a, &x->x_npoints, &x->x_vec))
  {
    pd_error(x, "%s: bad template for tabread4~~", x->x_arrayname->s_name);
    iemarray_setfloat(x->x_vec, 0, 0.0f);
  }
  else garray_usedindsp(a);
}

static void tabread4_tilde_tilde_dsp(t_tabread4_tilde_tilde *x, t_signal **sp)
{
  tabread4_tilde_tilde_set(x, x->x_arrayname);

  dsp_add(tabread4_tilde_tilde_perform, 5, x,
    sp[0]->s_vec, sp[1]->s_vec, sp[2]->s_vec, sp[0]->s_n);

}

static void tabread4_tilde_tilde_free(t_tabread4_tilde_tilde *x)
{
}

void tabread4_tilde_tilde_setup(void)
{
  tabread4_tilde_tilde_class = class_new(gensym("tabread4~~"),
    (t_newmethod)tabread4_tilde_tilde_new, (t_method)tabread4_tilde_tilde_free,
    sizeof(t_tabread4_tilde_tilde), 0, A_DEFSYM, 0);
  CLASS_MAINSIGNALIN(tabread4_tilde_tilde_class, t_tabread4_tilde_tilde, x_f);
  class_addmethod(tabread4_tilde_tilde_class, (t_method)tabread4_tilde_tilde_dsp, gensym("dsp"), A_CANT, 0);
  class_addmethod(tabread4_tilde_tilde_class, (t_method)tabread4_tilde_tilde_set, gensym("set"), A_SYMBOL, 0);
}
