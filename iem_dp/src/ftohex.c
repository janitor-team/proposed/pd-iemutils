/* For information on usage and redistribution, and for a DISCLAIMER OF ALL
* WARRANTIES, see the file, "LICENSE.txt," in this distribution.

iem_dp written by IOhannes m zmoelnig, Thomas Musil, Copyright (c) IEM KUG Graz Austria 1999 - 2007 */
/* double precision library */

#include "m_pd.h"
#include "iemlib.h"
#include "iem_dp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* -------------------------- ftohex ------------------------------ */
/* float to 8 digits of hexadecimal converter */

/* float (sign_1 + exp_8 + mant_23) */


static t_class *ftohex_class;

typedef struct _ftohex
{
  t_object  x_obj;
} t_ftohex;

static void ftohex_float(t_ftohex *x, t_floatarg f)
{
  char buf[100];
  union tabfudge_f tf;

  tf.tf_f = f;
  sprintf(buf, "#%08X", (unsigned int)tf.tf_l);
  outlet_symbol(x->x_obj.ob_outlet, gensym(buf));
}

static void *ftohex_new(void)
{
  t_ftohex *x = (t_ftohex *)pd_new(ftohex_class);
  outlet_new(&x->x_obj, &s_symbol);
  return (x);
}

void ftohex_setup(void)
{
  ftohex_class = class_new(gensym("ftohex"),
    (t_newmethod)ftohex_new, 0, sizeof(t_ftohex), 0, 0);
  class_addfloat(ftohex_class, ftohex_float);
}
