/* For information on usage and redistribution, and for a DISCLAIMER OF ALL
* WARRANTIES, see the file, "LICENSE.txt," in this distribution.

iem_dp written by IOhannes m zmoelnig, Thomas Musil, Copyright (c) IEM KUG Graz Austria 1999 - 2007 */
/* double precision library */

#include "m_pd.h"
#include "iemlib.h"
#include "iem_dp.h"


/* ------------------------  add__ or +__  ---------------------------- */
/* based on miller's +, which is part of pd, only with double precision */

static t_class *add___class;

typedef struct _add__
{
  t_object  x_obj;
  t_float   x_coarse_left;
  t_float   x_fine_left;
  t_float   x_coarse_right;
  t_float   x_fine_right;
  t_outlet  *x_out_coarse;
  t_outlet  *x_out_fine;
} t_add__;

static void add___bang(t_add__ *x)
{
  double dsum;
  t_float fsum;

  dsum = iem_dp_calc_sum(x->x_coarse_left, x->x_fine_left) + iem_dp_calc_sum(x->x_coarse_right, x->x_fine_right);
  fsum = iem_dp_cast_to_float(dsum);
  outlet_float(x->x_out_fine, iem_dp_calc_residual(dsum, fsum));
  outlet_float(x->x_out_coarse, fsum);
}

static void add___float(t_add__ *x, t_floatarg f)
{
  x->x_coarse_left = f;
  add___bang(x);
}

static void *add___new(t_symbol *s, int ac, t_atom *av)
{
  t_add__ *x = (t_add__ *)pd_new(add___class);

  floatinlet_new(&x->x_obj, &x->x_fine_left);
  floatinlet_new(&x->x_obj, &x->x_coarse_right);
  floatinlet_new(&x->x_obj, &x->x_fine_right);
  x->x_coarse_left = 0.0f;
  x->x_fine_left = 0.0f;
  if((ac > 0) && (IS_A_FLOAT(av, 0)))
    x->x_coarse_right = atom_getfloatarg(0, ac, av);
  else
    x->x_coarse_right = 0.0f;
  if((ac > 1) && (IS_A_FLOAT(av, 1)))
    x->x_fine_right = atom_getfloatarg(1, ac, av);
  else
    x->x_fine_right = 0.0f;
  x->x_out_coarse = outlet_new(&x->x_obj, &s_float);
  x->x_out_fine = outlet_new(&x->x_obj, &s_float);
  return (x);
}

void add___setup(void)
{
  add___class = class_new(gensym("add__"), (t_newmethod)add___new, 0, sizeof(t_add__), 0, A_GIMME, 0);
  class_addcreator((t_newmethod)add___new, gensym("+__"), A_GIMME, 0);
  class_addcreator((t_newmethod)add___new, gensym("+''"), A_GIMME, 0);
  class_addbang(add___class, add___bang);
  class_addfloat(add___class, add___float);
}
