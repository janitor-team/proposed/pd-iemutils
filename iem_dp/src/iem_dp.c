/* For information on usage and redistribution, and for a DISCLAIMER OF ALL
 * WARRANTIES, see the file, "LICENSE.txt," in this distribution.

 * iem_dp written by IOhannes m zmoelnig, Thomas Musil,
 * Copyright (c) IEM KUG Graz Austria 2000 - 2008 */

/* iem_dp means double precision, it is a library of external pd objects */
/* which deal with array access and delay lines */
/* the way how objects comunicate with other objects in double precision is, */
/* to send two values on two different outputs to two different inputs */
/* one value is the float casted value of the original double value */
/* the other value is the difference in double of the original minus the float casted value */
/* the receiving inlets only cast them to double and add them */


#include "m_pd.h"
#include "iemlib.h"

//static int delread_zero = 0;    /* four bytes of zero for delread~, vd~ */

t_float iem_dp_cast_to_float(double d)
{
  return((t_float)d);
}

double iem_dp_cast_to_double(t_float f)
{
  return((double)f);
}

t_float iem_dp_calc_residual(double d, t_float f)
{
  return(iem_dp_cast_to_float(d - iem_dp_cast_to_double(f)));
}

double iem_dp_calc_sum(t_float f, t_float r)
{
  return(iem_dp_cast_to_double(f) + iem_dp_cast_to_double(r));
}

static t_class *iem_dp_class;

static void *iem_dp_new(void)
{
  t_object *x = (t_object *)pd_new(iem_dp_class);

  return (x);
}


void ftohex_setup(void);
void symtodp_setup(void);
void dptosym_setup(void);
void dptohex_setup(void);
void vline_tilde_tilde_setup(void);
void samphold_tilde_tilde_setup(void);
void wrap_tilde_tilde_setup(void);
void phasor_tilde_tilde_setup(void);
void print_tilde_tilde_setup(void);
void add___setup(void);
void sub___setup(void);
void mul___setup(void);
void div___setup(void);
void add_tilde_tilde_setup(void);
void sub_tilde_tilde_setup(void);
void mul_tilde_tilde_setup(void);
void div_tilde_tilde_setup(void);
void tabwrite_dp_setup(void);
void tabread_dp_setup(void);
void tabread4_dp_setup(void);
void tabwrite_tilde_tilde_setup(void);
void tabread_tilde_tilde_setup(void);
void tabread4_tilde_tilde_setup(void);
void max_dp_setup(void);
void min_dp_setup(void);
void max_tilde_tilde_setup(void);
void min_tilde_tilde_setup(void);
void random_dp_setup(void);
void delay_tilde_tilde_setup(void);
//void listtodp_setup(void);
//void dqnsymtodp_setup(void);
//void dptodqnsym_setup(void);

/* ------------------------ setup routine ------------------------- */

void iem_dp_setup(void)
{
  iem_dp_class = class_new(gensym("iem_dp"), iem_dp_new, 0,
    sizeof(t_object), CLASS_NOINLET, 0);

  ftohex_setup();
  symtodp_setup();
  dptosym_setup();
  dptohex_setup();
  vline_tilde_tilde_setup();
  samphold_tilde_tilde_setup();
  wrap_tilde_tilde_setup();
  phasor_tilde_tilde_setup();
  print_tilde_tilde_setup();
  add___setup();
  sub___setup();
  mul___setup();
  div___setup();
  add_tilde_tilde_setup();
  sub_tilde_tilde_setup();
  mul_tilde_tilde_setup();
  div_tilde_tilde_setup();
  tabwrite_dp_setup();
  tabread_dp_setup();
  tabread4_dp_setup();
  tabwrite_tilde_tilde_setup();
  tabread_tilde_tilde_setup();
  tabread4_tilde_tilde_setup();
  max_dp_setup();
  min_dp_setup();
  max_tilde_tilde_setup();
  min_tilde_tilde_setup();
  random_dp_setup();
  delay_tilde_tilde_setup();
//  listtodp_setup();
//  dqnsymtodp_setup();
//  dptodqnsym_setup();

        post("iem_dp (R-1.19) library loaded!   (c) IOhannes m zmoelnig, Thomas Musil 06.2013");
        post("   zmoelnig%ciem.at iem KUG Graz Austria", '@');
  post("   musil%ciem.at iem KUG Graz Austria", '@');
}
