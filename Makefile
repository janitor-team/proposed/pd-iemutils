subprojects = iem_adaptfilt iem_dp iem_roomsim iem_spec2 iem_tab punish
all: $(subprojects)

.PHONY: all clean archive release submodule update dummy
.PHONY: $(subprojects)


library=iem_utils
version=0.0.$(shell date +%Y%m%d)
archivefile=$(library)-v$(version).tgz

IEM_CFLAGS=-DPD $(CPPFLAGS) -fPIC $(CFLAGS)
IEM_LDFLAGS=-export-dynamic -fPIC -shared $(LDFLAGS)

clean:
	-find . -name "*.o" -delete
	-find . -name "*.pd_*" -delete
	-find . -name "*.dll" -delete
	-find . -name "*.tgz" -delete

dummy:
	@echo -n

archive: $(archivefile)

release: $(archivefile)
	git tag -m "released iem_utils $(version)" "v$(version)"

submodule: .gitmodules
	git submodule init
	git submodule update
update:
	git submodule foreach git checkout master
	git submodule foreach git pull

%.tgz: dummy
	-rm -f $@
	tar --transform "s|^|$(@:.tgz=)/|" --exclude-vcs --exclude='.*' --exclude='*.tgz' --exclude $@ -czf $@ *

iem_adaptfilt iem_dp iem_roomsim iem_spec2 iem_tab punish:
	$(MAKE) -C $@ \
		$(empty)
