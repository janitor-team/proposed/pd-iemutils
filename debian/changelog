pd-iemutils (0.0.20181004-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Fix FTCBFS: Let dpkg's buildtools.mk initialize CC and friends.
    Thanks to Helmut Grohne <helmut@subdivi.de> (Closes: #958134)
  * Add salsa-ci configuration
  * Remove obsolete file d/source/local-options
  * Bump dh-compat to 12
  * Bump standards version to 4.5.0

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 20 Apr 2020 18:32:45 +0200

pd-iemutils (0.0.20181004-1) unstable; urgency=medium

  * New upstream version 0.0.20181004
  * Updated d/copyright(|_hints)
  * Don't require root for building
  * Bumped standards version to 4.2.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 04 Oct 2018 22:26:16 +0200

pd-iemutils (0.0.20180529-1) unstable; urgency=medium

  * New upstream version 0.0.20180529 (Closes: #890572)
  * Bumped standards version to 4.1.4

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 29 May 2018 21:35:38 +0200

pd-iemutils (0.0.20180206-1) unstable; urgency=medium

  * New upstream version 0.0.20180206
    * Provides iem16 and doublechord
    * Dropped patches applied upstream

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 06 Feb 2018 12:50:12 +0100

pd-iemutils (0.0.20161027-3) unstable; urgency=medium

  * Use system-wide pd-lib-builder
  * Don't fail when building arch:all (Closes: #889629)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 05 Feb 2018 14:33:52 +0100

pd-iemutils (0.0.20161027-2) unstable; urgency=medium

  * Switched buildsystem from dh to cdbs
    * Bumped dh compat to 11
    * Enabled hardening
    * Dropped unneeded B-Ds
  * Updated Vcs-* stanzas to salsa.d.o
  * Updated maintainer address
  * Removed trailing whitespace in debian/*
  * Added DEP3 header to patches
  * Switched URLs to https://
  * Removed obsolete d/git-tuneclone.sh
  * Updated d/copyright_hints
  * Bumped standards version to 4.1.3

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 01 Feb 2018 23:15:47 +0100

pd-iemutils (0.0.20161027-1) unstable; urgency=medium

  * New upstream version 0.0.20161027

  * Refreshed patches
    * Adjusted FLAGS
  * Install punish plugins
    * Mention patcherize/triggerize in description
  * Mention pd-gui-plugin for enabling gui-plugins
  * Updated d/copyright
  * Regenerated d/(control|copyright_hints)
  * Bumped standards-version to 3.9.8

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 27 Oct 2016 17:17:22 +0200

pd-iemutils (0.0.20160217-2) unstable; urgency=medium

  * Fixed path for GUI plugins
        /usr/share/pd-gui/plugins-available

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 17 Feb 2016 20:29:38 +0100

pd-iemutils (0.0.20160217-1) unstable; urgency=medium

  * Imported Upstream version 0.0.20160217
    * Refreshed patches
    * Installed renamed README
  * Fixed debian/watch
  * Added GUI plugins to pd-iemutils package
    * GUI plugins get installed into /usr/share/pd-gui/available-plugins/
    * Updated package description
    * Documented enabling of GUI plugins in README.Debian
  * Updated debian/copyright
    * Regeneratd debian/copyright_hints
  * Use https:// in Vcs-Git stanza
  * Bumped standards version to 3.9.7

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 17 Feb 2016 16:46:22 +0100

pd-iemutils (0.0.20151207-1) unstable; urgency=medium

  * Initial release (Closes: #795901)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 07 Dec 2015 21:40:24 +0100
