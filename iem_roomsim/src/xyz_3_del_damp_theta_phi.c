/* For information on usage and redistribution, and for a DISCLAIMER OF ALL
* WARRANTIES, see the file, "LICENSE.txt," in this distribution.

iem_roomsim written by Thomas Musil (c) IEM KUG Graz Austria 2002 - 2018 */

#include "m_pd.h"
#include "iemlib.h"
#include <math.h>


/* -------------------------- xyz_3_del_damp_theta_phi ------------------------------ */
/*
**  pos. x-Richtung Nase
**  pos. y-Richtung Linke Hand
**  pos. z-Richtung Scheitel
**  Kartesischer Koordinaten-Ursprung liegt in der Mitte des Raums am Boden

  aenderungen: src-index von 1 .. n auf 0 .. (n-1)
  aenderungen: azimuth von rad auf degree
*/

/*

    
    1., 2., 3. und 4. reflexionen:


      top   +z

            +y
             ^
             |
             |
             |
             |
             |
             |
            103 ---------------> +x

            +y
             ^
             |
             |
             |
             |
             |
            100
         101 49 99 ------------> +x
            102

            +y
             ^
             |
             |
             |
             |
             93
          94 46 92
       95 47 19 45 91 ---------> +x
          96 48 98
             97

            +y
             ^
             |
             |
             |
             82
          83 39 81
       84 40 16 38 80
    85 41 17 05 15 37 79 ------> +x
       86 42 18 44 90
          87 43 89
             88

            +y
             ^
             |
             |
             67
          68 28 66
       69 29 09 27 65
    70 30 10 02 08 26 64
 71 31 11 03 00 01 07 25 63 ---> +x
    72 32 12 04 14 36 78
       73 33 13 35 77
          74 34 76
             75

            +y
             ^
             |
             |
             |
            107
         108 52 106
      109 53 21 51 105
   110 54 22 06 20 50 104 -----> +x
      111 55 23 57 115
         112 56 114
            113

            +y
             ^
             |
             |
             |
             |
            118
         119 59 117
      120 60 24 58 116 --------> +x
         121 61 123
            122

            +y
             ^
             |
             |
             |
             |
             |
            125
         126 62 124 -----------> +x
            127

            +y
             ^
             |
             |
             |
             |
             |
             |
            128 ---------------> +x

   bottom   -z


*/



typedef struct _xyz_3_del_damp_theta_phi
{
  t_object  x_obj;
  t_symbol  *x_s_direct;
  t_symbol  *x_s_early1;
  t_symbol  *x_s_early2;
  t_symbol  *x_s_early3;
  t_symbol  *x_s_early4;
  t_symbol  *x_s_del;
  t_symbol  *x_s_damp;
  t_symbol  *x_s_theta;
  t_symbol  *x_s_phi;
  t_float   x_room_x;
  t_float   x_room_y;
  t_float   x_room_z;
  t_float   x_head_x;
  t_float   x_head_y;
  t_float   x_head_z;
  t_float   x_src_x;
  t_float   x_src_y;
  t_float   x_src_z;
  t_float   x_r_min;
  t_float   x_speed;
  t_float   x_180_over_pi;
  t_float   *x_rad;
  t_atom    *x_at;
  t_int     x_size_rad;
  t_int     x_size_at;
  void      *x_clock;
} t_xyz_3_del_damp_theta_phi;

static t_class *xyz_3_del_damp_theta_phi_class;

static t_float xyz_3_del_damp_theta_phi_calc_radius(t_float r_min, t_float dx, t_float dy, t_float dz)
{
  t_float r = (t_float)sqrt(dx*dx + dy*dy + dz*dz);
  
  if(r < r_min)
    return(r_min);
  else
    return(r);
}

static t_float xyz_3_del_damp_theta_phi_calc_azimuth(t_float x_180_over_pi, t_float dx, t_float dy, t_float dz)
{
  if(dx == 0.0f)
  {
    if(dy < 0.0f)
      return(270.0f);
    else
      return(90.0f);
  }
  else if(dx < 0.0f)
  {
    return(180.0f + x_180_over_pi * (t_float)atan(dy / dx));
  }
  else
  {
    if(dy < 0.0f)
      return(360.0f + x_180_over_pi * (t_float)atan(dy / dx));
    else
      return(x_180_over_pi * (t_float)atan(dy / dx));
  }
}

static t_float xyz_3_del_damp_theta_phi_calc_elevation(t_float x_180_over_pi, t_float dx, t_float dy, t_float dz)
{
  t_float dxy = sqrt(dx*dx + dy*dy);
  
  if(dxy == 0.0f)
  {
    if(dz < 0.0f)
      return(-90.0f);
    else
      return(90.0f);
  }
  else
  {
    return(x_180_over_pi * (t_float)atan(dz / dxy));
  }
}

static void xyz_3_del_damp_theta_phi_doit(t_xyz_3_del_damp_theta_phi *x)
{
  t_float diff_x, diff_y, diff_z;
  t_float sum_x, sum_y, sum_z;
  t_float lx, wy, hz;
  t_float x_0, y_0, z_0;
  t_float xp1, yp1, zp1, xn1, yn1, zn1;
  t_float xp2, yp2, zp2, xn2, yn2, zn2;
  t_float xp3, yp3, zp3, xn3, yn3, zn3;
  t_float xp4, yp4, zp4, xn4, yn4, zn4;
  t_float m2ms = 1000.0f / x->x_speed;
  t_float x_180_over_pi=x->x_180_over_pi;
  t_float r_min = x->x_r_min;
  t_float *rad=x->x_rad;
  t_atom *at=x->x_at;
  
  lx = x->x_room_x;
  wy = x->x_room_y;
  hz = x->x_room_z;
  
  diff_x = x->x_src_x - x->x_head_x;
  diff_y = x->x_src_y - x->x_head_y;
  diff_z = x->x_src_z - x->x_head_z;
  sum_x = x->x_src_x + x->x_head_x;
  sum_y = x->x_src_y + x->x_head_y;
  sum_z = x->x_src_z + x->x_head_z - hz;
  
  x_0 = diff_x;
  y_0 = diff_y;
  z_0 = diff_z;
  xp1 = lx - sum_x;
  yp1 = wy - sum_y;
  zp1 = hz - sum_z;
  xn1 = -lx - sum_x;
  yn1 = -wy - sum_y;
  zn1 = -hz - sum_z;
  xp2 = 2.0f*lx + diff_x;
  yp2 = 2.0f*wy + diff_y;
  zp2 = 2.0f*hz + diff_z;
  xn2 = -2.0f*lx + diff_x;
  yn2 = -2.0f*wy + diff_y;
  zn2 = -2.0f*hz + diff_z;
  xp3 = 3.0f*lx - sum_x;
  yp3 = 3.0f*wy - sum_y;
  zp3 = 3.0f*hz - sum_z;
  xn3 = -3.0f*lx - sum_x;
  yn3 = -3.0f*wy - sum_y;
  zn3 = -3.0f*hz - sum_z;
  xp4 = 4.0f*lx + diff_x;
  yp4 = 4.0f*wy + diff_y;
  zp4 = 4.0f*hz + diff_z;
  xn4 = -4.0f*lx + diff_x;
  yn4 = -4.0f*wy + diff_y;
  zn4 = -4.0f*hz + diff_z;
  
  rad[0] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, y_0, z_0);

  rad[1] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, y_0, z_0);
  rad[2] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yp1, z_0);
  rad[3] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, y_0, z_0);
  rad[4] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yn1, z_0);
  rad[5] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, y_0, zp1);
  rad[6] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, y_0, zn1);

  rad[7] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp2, y_0, z_0);
  rad[8] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yp1, z_0);
  rad[9] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yp2, z_0);
  rad[10] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yp1, z_0);
  rad[11] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn2, y_0, z_0);
  rad[12] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yn1, z_0);
  rad[13] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yn2, z_0);
  rad[14] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yn1, z_0);
  rad[15] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, y_0, zp1);
  rad[16] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yp1, zp1);
  rad[17] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, y_0, zp1);
  rad[18] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yn1, zp1);
  rad[19] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, y_0, zp2);
  rad[20] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, y_0, zn1);
  rad[21] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yp1, zn1);
  rad[22] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, y_0, zn1);
  rad[23] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yn1, zn1);
  rad[24] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, y_0, zn2);

  rad[25] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp3, y_0, z_0);
  rad[26] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp2, yp1, z_0);
  rad[27] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yp2, z_0);
  rad[28] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yp3, z_0);
  rad[29] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yp2, z_0);
  rad[30] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn2, yp1, z_0);
  rad[31] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn3, y_0, z_0);
  rad[32] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn2, yn1, z_0);
  rad[33] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yn2, z_0);
  rad[34] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yn3, z_0);
  rad[35] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yn2, z_0);
  rad[36] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp2, yn1, z_0);
  rad[37] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp2, y_0, zp1);
  rad[38] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yp1, zp1);
  rad[39] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yp2, zp1);
  rad[40] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yp1, zp1);
  rad[41] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn2, y_0, zp1);
  rad[42] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yn1, zp1);
  rad[43] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yn2, zp1);
  rad[44] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yn1, zp1);
  rad[45] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, y_0, zp2);
  rad[46] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yp1, zp2);
  rad[47] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, y_0, zp2);
  rad[48] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yn1, zp2);
  rad[49] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, y_0, zp3);
  rad[50] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp2, y_0, zn1);
  rad[51] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yp1, zn1);
  rad[52] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yp2, zn1);
  rad[53] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yp1, zn1);
  rad[54] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn2, y_0, zn1);
  rad[55] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yn1, zn1);
  rad[56] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yn2, zn1);
  rad[57] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yn1, zn1);
  rad[58] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, y_0, zn2);
  rad[59] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yp1, zn2);
  rad[60] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, y_0, zn2);
  rad[61] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yn1, zn2);
  rad[62] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, y_0, zn3);

  rad[63] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp4, y_0, z_0);
  rad[64] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp3, yp1, z_0);
  rad[65] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp2, yp2, z_0);
  rad[66] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yp3, z_0);
  rad[67] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yp4, z_0);
  rad[68] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yp3, z_0);
  rad[69] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn2, yp2, z_0);
  rad[70] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn3, yp1, z_0);
  rad[71] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn4, y_0, z_0);
  rad[72] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn3, yn1, z_0);
  rad[73] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn2, yn2, z_0);
  rad[74] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yn3, z_0);
  rad[75] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yn4, z_0);
  rad[76] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yn3, z_0);
  rad[77] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp2, yn2, z_0);
  rad[78] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp3, yn1, z_0);
  rad[79] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp3, y_0, zp1);
  rad[80] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp2, yp1, zp1);
  rad[81] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yp2, zp1);
  rad[82] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yp3, zp1);
  rad[83] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yp2, zp1);
  rad[84] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn2, yp1, zp1);
  rad[85] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn3, y_0, zp1);
  rad[86] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn2, yn1, zp1);
  rad[87] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yn2, zp1);
  rad[88] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yn3, zp1);
  rad[89] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yn2, zp1);
  rad[90] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp2, yn1, zp1);
  rad[91] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp2, y_0, zp2);
  rad[92] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yp1, zp2);
  rad[93] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yp2, zp2);
  rad[94] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yp1, zp2);
  rad[95] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn2, y_0, zp2);
  rad[96] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yn1, zp2);
  rad[97] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yn2, zp2);
  rad[98] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yn1, zp2);
  rad[99] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, y_0, zp3);
  rad[100] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yp1, zp3);
  rad[101] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, y_0, zp3);
  rad[102] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yn1, zp3);
  rad[103] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, y_0, zp4);
  rad[104] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp3, y_0, zn1);
  rad[105] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp2, yp1, zn1);
  rad[106] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yp2, zn1);
  rad[107] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yp3, zn1);
  rad[108] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yp2, zn1);
  rad[109] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn2, yp1, zn1);
  rad[110] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn3, y_0, zn1);
  rad[111] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn2, yn1, zn1);
  rad[112] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yn2, zn1);
  rad[113] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yn3, zn1);
  rad[114] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yn2, zn1);
  rad[115] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp2, yn1, zn1);
  rad[116] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp2, y_0, zn2);
  rad[117] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yp1, zn2);
  rad[118] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yp2, zn2);
  rad[119] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yp1, zn2);
  rad[120] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn2, y_0, zn2);
  rad[121] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, yn1, zn2);
  rad[122] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yn2, zn2);
  rad[123] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, yn1, zn2);
  rad[124] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xp1, y_0, zn3);
  rad[125] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yp1, zn3);
  rad[126] = xyz_3_del_damp_theta_phi_calc_radius(r_min, xn1, y_0, zn3);
  rad[127] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, yn1, zn3);
  rad[128] = xyz_3_del_damp_theta_phi_calc_radius(r_min, x_0, y_0, zn4);
  


  SETSYMBOL(at, x->x_s_del);
  
  SETFLOAT(at+1, rad[0] * m2ms);
  outlet_anything(x->x_obj.ob_outlet, x->x_s_direct, 2, at);
  
  SETFLOAT(at+1, rad[1] * m2ms);
  SETFLOAT(at+2, rad[2] * m2ms);
  SETFLOAT(at+3, rad[3] * m2ms);
  SETFLOAT(at+4, rad[4] * m2ms);
  SETFLOAT(at+5, rad[5] * m2ms);
  SETFLOAT(at+6, rad[6] * m2ms);
  outlet_anything(x->x_obj.ob_outlet, x->x_s_early1, 7, at);
  
  SETFLOAT(at+1, rad[7] * m2ms);
  SETFLOAT(at+2, rad[8] * m2ms);
  SETFLOAT(at+3, rad[9] * m2ms);
  SETFLOAT(at+4, rad[10] * m2ms);
  SETFLOAT(at+5, rad[11] * m2ms);
  SETFLOAT(at+6, rad[12] * m2ms);
  SETFLOAT(at+7, rad[13] * m2ms);
  SETFLOAT(at+8, rad[14] * m2ms);
  SETFLOAT(at+9, rad[15] * m2ms);
  SETFLOAT(at+10, rad[16] * m2ms);
  SETFLOAT(at+11, rad[17] * m2ms);
  SETFLOAT(at+12, rad[18] * m2ms);
  SETFLOAT(at+13, rad[19] * m2ms);
  SETFLOAT(at+14, rad[20] * m2ms);
  SETFLOAT(at+15, rad[21] * m2ms);
  SETFLOAT(at+16, rad[22] * m2ms);
  SETFLOAT(at+17, rad[23] * m2ms);
  SETFLOAT(at+18, rad[24] * m2ms);
  outlet_anything(x->x_obj.ob_outlet, x->x_s_early2, 19, at);

  SETFLOAT(at+1, rad[25] * m2ms);
  SETFLOAT(at+2, rad[26] * m2ms);
  SETFLOAT(at+3, rad[27] * m2ms);
  SETFLOAT(at+4, rad[28] * m2ms);
  SETFLOAT(at+5, rad[29] * m2ms);
  SETFLOAT(at+6, rad[30] * m2ms);
  SETFLOAT(at+7, rad[31] * m2ms);
  SETFLOAT(at+8, rad[32] * m2ms);
  SETFLOAT(at+9, rad[33] * m2ms);
  SETFLOAT(at+10, rad[34] * m2ms);
  SETFLOAT(at+11, rad[35] * m2ms);
  SETFLOAT(at+12, rad[36] * m2ms);
  SETFLOAT(at+13, rad[37] * m2ms);
  SETFLOAT(at+14, rad[38] * m2ms);
  SETFLOAT(at+15, rad[39] * m2ms);
  SETFLOAT(at+16, rad[40] * m2ms);
  SETFLOAT(at+17, rad[41] * m2ms);
  SETFLOAT(at+18, rad[42] * m2ms);
  SETFLOAT(at+19, rad[43] * m2ms);
  SETFLOAT(at+20, rad[44] * m2ms);
  SETFLOAT(at+21, rad[45] * m2ms);
  SETFLOAT(at+22, rad[46] * m2ms);
  SETFLOAT(at+23, rad[47] * m2ms);
  SETFLOAT(at+24, rad[48] * m2ms);
  SETFLOAT(at+25, rad[49] * m2ms);
  SETFLOAT(at+26, rad[50] * m2ms);
  SETFLOAT(at+27, rad[51] * m2ms);
  SETFLOAT(at+28, rad[52] * m2ms);
  SETFLOAT(at+29, rad[53] * m2ms);
  SETFLOAT(at+30, rad[54] * m2ms);
  SETFLOAT(at+31, rad[55] * m2ms);
  SETFLOAT(at+32, rad[56] * m2ms);
  SETFLOAT(at+33, rad[57] * m2ms);
  SETFLOAT(at+34, rad[58] * m2ms);
  SETFLOAT(at+35, rad[59] * m2ms);
  SETFLOAT(at+36, rad[60] * m2ms);
  SETFLOAT(at+37, rad[61] * m2ms);
  SETFLOAT(at+38, rad[62] * m2ms);
  outlet_anything(x->x_obj.ob_outlet, x->x_s_early3, 39, at);

  SETFLOAT(at+1, rad[63] * m2ms);
  SETFLOAT(at+2, rad[64] * m2ms);
  SETFLOAT(at+3, rad[65] * m2ms);
  SETFLOAT(at+4, rad[66] * m2ms);
  SETFLOAT(at+5, rad[67] * m2ms);
  SETFLOAT(at+6, rad[68] * m2ms);
  SETFLOAT(at+7, rad[69] * m2ms);
  SETFLOAT(at+8, rad[70] * m2ms);
  SETFLOAT(at+9, rad[71] * m2ms);
  SETFLOAT(at+10, rad[72] * m2ms);
  SETFLOAT(at+11, rad[73] * m2ms);
  SETFLOAT(at+12, rad[74] * m2ms);
  SETFLOAT(at+13, rad[75] * m2ms);
  SETFLOAT(at+14, rad[76] * m2ms);
  SETFLOAT(at+15, rad[77] * m2ms);
  SETFLOAT(at+16, rad[78] * m2ms);
  SETFLOAT(at+17, rad[79] * m2ms);
  SETFLOAT(at+18, rad[80] * m2ms);
  SETFLOAT(at+19, rad[81] * m2ms);
  SETFLOAT(at+20, rad[82] * m2ms);
  SETFLOAT(at+21, rad[83] * m2ms);
  SETFLOAT(at+22, rad[84] * m2ms);
  SETFLOAT(at+23, rad[85] * m2ms);
  SETFLOAT(at+24, rad[86] * m2ms);
  SETFLOAT(at+25, rad[87] * m2ms);
  SETFLOAT(at+26, rad[88] * m2ms);
  SETFLOAT(at+27, rad[89] * m2ms);
  SETFLOAT(at+28, rad[90] * m2ms);
  SETFLOAT(at+29, rad[91] * m2ms);
  SETFLOAT(at+30, rad[92] * m2ms);
  SETFLOAT(at+31, rad[93] * m2ms);
  SETFLOAT(at+32, rad[94] * m2ms);
  SETFLOAT(at+33, rad[95] * m2ms);
  SETFLOAT(at+34, rad[96] * m2ms);
  SETFLOAT(at+35, rad[97] * m2ms);
  SETFLOAT(at+36, rad[98] * m2ms);
  SETFLOAT(at+37, rad[99] * m2ms);
  SETFLOAT(at+38, rad[100] * m2ms);
  SETFLOAT(at+39, rad[101] * m2ms);
  SETFLOAT(at+40, rad[102] * m2ms);
  SETFLOAT(at+41, rad[103] * m2ms);
  SETFLOAT(at+42, rad[104] * m2ms);
  SETFLOAT(at+43, rad[105] * m2ms);
  SETFLOAT(at+44, rad[106] * m2ms);
  SETFLOAT(at+45, rad[107] * m2ms);
  SETFLOAT(at+46, rad[108] * m2ms);
  SETFLOAT(at+47, rad[109] * m2ms);
  SETFLOAT(at+48, rad[110] * m2ms);
  SETFLOAT(at+49, rad[111] * m2ms);
  SETFLOAT(at+50, rad[112] * m2ms);
  SETFLOAT(at+51, rad[113] * m2ms);
  SETFLOAT(at+52, rad[114] * m2ms);
  SETFLOAT(at+53, rad[115] * m2ms);
  SETFLOAT(at+54, rad[116] * m2ms);
  SETFLOAT(at+55, rad[117] * m2ms);
  SETFLOAT(at+56, rad[118] * m2ms);
  SETFLOAT(at+57, rad[119] * m2ms);
  SETFLOAT(at+58, rad[120] * m2ms);
  SETFLOAT(at+59, rad[121] * m2ms);
  SETFLOAT(at+60, rad[122] * m2ms);
  SETFLOAT(at+61, rad[123] * m2ms);
  SETFLOAT(at+62, rad[124] * m2ms);
  SETFLOAT(at+63, rad[125] * m2ms);
  SETFLOAT(at+64, rad[126] * m2ms);
  SETFLOAT(at+65, rad[127] * m2ms);
  SETFLOAT(at+66, rad[128] * m2ms);
  outlet_anything(x->x_obj.ob_outlet, x->x_s_early4, 67, at);
  
  
  
  SETSYMBOL(at, x->x_s_damp);
  
  SETFLOAT(at+1, r_min / rad[0]);
  outlet_anything(x->x_obj.ob_outlet, x->x_s_direct, 2, at);
  
  SETFLOAT(at+1, r_min / rad[1]);
  SETFLOAT(at+2, r_min / rad[2]);
  SETFLOAT(at+3, r_min / rad[3]);
  SETFLOAT(at+4, r_min / rad[4]);
  SETFLOAT(at+5, r_min / rad[5]);
  SETFLOAT(at+6, r_min / rad[6]);
  outlet_anything(x->x_obj.ob_outlet, x->x_s_early1, 7, at);

  SETFLOAT(at+1, r_min / rad[7]);
  SETFLOAT(at+2, r_min / rad[8]);
  SETFLOAT(at+3, r_min / rad[9]);
  SETFLOAT(at+4, r_min / rad[10]);
  SETFLOAT(at+5, r_min / rad[11]);
  SETFLOAT(at+6, r_min / rad[12]);
  SETFLOAT(at+7, r_min / rad[13]);
  SETFLOAT(at+8, r_min / rad[14]);
  SETFLOAT(at+9, r_min / rad[15]);
  SETFLOAT(at+10, r_min / rad[16]);
  SETFLOAT(at+11, r_min / rad[17]);
  SETFLOAT(at+12, r_min / rad[18]);
  SETFLOAT(at+13, r_min / rad[19]);
  SETFLOAT(at+14, r_min / rad[20]);
  SETFLOAT(at+15, r_min / rad[21]);
  SETFLOAT(at+16, r_min / rad[22]);
  SETFLOAT(at+17, r_min / rad[23]);
  SETFLOAT(at+18, r_min / rad[24]);
  outlet_anything(x->x_obj.ob_outlet, x->x_s_early2, 19, at);

  SETFLOAT(at+1, r_min / rad[25]);
  SETFLOAT(at+2, r_min / rad[26]);
  SETFLOAT(at+3, r_min / rad[27]);
  SETFLOAT(at+4, r_min / rad[28]);
  SETFLOAT(at+5, r_min / rad[29]);
  SETFLOAT(at+6, r_min / rad[30]);
  SETFLOAT(at+7, r_min / rad[31]);
  SETFLOAT(at+8, r_min / rad[32]);
  SETFLOAT(at+9, r_min / rad[33]);
  SETFLOAT(at+10, r_min / rad[34]);
  SETFLOAT(at+11, r_min / rad[35]);
  SETFLOAT(at+12, r_min / rad[36]);
  SETFLOAT(at+13, r_min / rad[37]);
  SETFLOAT(at+14, r_min / rad[38]);
  SETFLOAT(at+15, r_min / rad[39]);
  SETFLOAT(at+16, r_min / rad[40]);
  SETFLOAT(at+17, r_min / rad[41]);
  SETFLOAT(at+18, r_min / rad[42]);
  SETFLOAT(at+19, r_min / rad[43]);
  SETFLOAT(at+20, r_min / rad[44]);
  SETFLOAT(at+21, r_min / rad[45]);
  SETFLOAT(at+22, r_min / rad[46]);
  SETFLOAT(at+23, r_min / rad[47]);
  SETFLOAT(at+24, r_min / rad[48]);
  SETFLOAT(at+25, r_min / rad[49]);
  SETFLOAT(at+26, r_min / rad[50]);
  SETFLOAT(at+27, r_min / rad[51]);
  SETFLOAT(at+28, r_min / rad[52]);
  SETFLOAT(at+29, r_min / rad[53]);
  SETFLOAT(at+30, r_min / rad[54]);
  SETFLOAT(at+31, r_min / rad[55]);
  SETFLOAT(at+32, r_min / rad[56]);
  SETFLOAT(at+33, r_min / rad[57]);
  SETFLOAT(at+34, r_min / rad[58]);
  SETFLOAT(at+35, r_min / rad[59]);
  SETFLOAT(at+36, r_min / rad[60]);
  SETFLOAT(at+37, r_min / rad[61]);
  SETFLOAT(at+38, r_min / rad[62]);
  outlet_anything(x->x_obj.ob_outlet, x->x_s_early3, 39, at);

  SETFLOAT(at+1, r_min / rad[63]);
  SETFLOAT(at+2, r_min / rad[64]);
  SETFLOAT(at+3, r_min / rad[65]);
  SETFLOAT(at+4, r_min / rad[66]);
  SETFLOAT(at+5, r_min / rad[67]);
  SETFLOAT(at+6, r_min / rad[68]);
  SETFLOAT(at+7, r_min / rad[69]);
  SETFLOAT(at+8, r_min / rad[70]);
  SETFLOAT(at+9, r_min / rad[71]);
  SETFLOAT(at+10, r_min / rad[72]);
  SETFLOAT(at+11, r_min / rad[73]);
  SETFLOAT(at+12, r_min / rad[74]);
  SETFLOAT(at+13, r_min / rad[75]);
  SETFLOAT(at+14, r_min / rad[76]);
  SETFLOAT(at+15, r_min / rad[77]);
  SETFLOAT(at+16, r_min / rad[78]);
  SETFLOAT(at+17, r_min / rad[79]);
  SETFLOAT(at+18, r_min / rad[80]);
  SETFLOAT(at+19, r_min / rad[81]);
  SETFLOAT(at+20, r_min / rad[82]);
  SETFLOAT(at+21, r_min / rad[83]);
  SETFLOAT(at+22, r_min / rad[84]);
  SETFLOAT(at+23, r_min / rad[85]);
  SETFLOAT(at+24, r_min / rad[86]);
  SETFLOAT(at+25, r_min / rad[87]);
  SETFLOAT(at+26, r_min / rad[88]);
  SETFLOAT(at+27, r_min / rad[89]);
  SETFLOAT(at+28, r_min / rad[90]);
  SETFLOAT(at+29, r_min / rad[91]);
  SETFLOAT(at+30, r_min / rad[92]);
  SETFLOAT(at+31, r_min / rad[93]);
  SETFLOAT(at+32, r_min / rad[94]);
  SETFLOAT(at+33, r_min / rad[95]);
  SETFLOAT(at+34, r_min / rad[96]);
  SETFLOAT(at+35, r_min / rad[97]);
  SETFLOAT(at+36, r_min / rad[98]);
  SETFLOAT(at+37, r_min / rad[99]);
  SETFLOAT(at+38, r_min / rad[100]);
  SETFLOAT(at+39, r_min / rad[101]);
  SETFLOAT(at+40, r_min / rad[102]);
  SETFLOAT(at+41, r_min / rad[103]);
  SETFLOAT(at+42, r_min / rad[104]);
  SETFLOAT(at+43, r_min / rad[105]);
  SETFLOAT(at+44, r_min / rad[106]);
  SETFLOAT(at+45, r_min / rad[107]);
  SETFLOAT(at+46, r_min / rad[108]);
  SETFLOAT(at+47, r_min / rad[109]);
  SETFLOAT(at+48, r_min / rad[110]);
  SETFLOAT(at+49, r_min / rad[111]);
  SETFLOAT(at+50, r_min / rad[112]);
  SETFLOAT(at+51, r_min / rad[113]);
  SETFLOAT(at+52, r_min / rad[114]);
  SETFLOAT(at+53, r_min / rad[115]);
  SETFLOAT(at+54, r_min / rad[116]);
  SETFLOAT(at+55, r_min / rad[117]);
  SETFLOAT(at+56, r_min / rad[118]);
  SETFLOAT(at+57, r_min / rad[119]);
  SETFLOAT(at+58, r_min / rad[120]);
  SETFLOAT(at+59, r_min / rad[121]);
  SETFLOAT(at+60, r_min / rad[122]);
  SETFLOAT(at+61, r_min / rad[123]);
  SETFLOAT(at+62, r_min / rad[124]);
  SETFLOAT(at+63, r_min / rad[125]);
  SETFLOAT(at+64, r_min / rad[126]);
  SETFLOAT(at+65, r_min / rad[127]);
  SETFLOAT(at+66, r_min / rad[128]);
  outlet_anything(x->x_obj.ob_outlet, x->x_s_early4, 67, at);
  
  
  
  
  SETSYMBOL(at, x->x_s_theta);

  SETFLOAT(at+1, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, y_0, z_0));
  outlet_anything(x->x_obj.ob_outlet, x->x_s_direct, 2, at);
  
  SETFLOAT(at+1, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, y_0, z_0));
  SETFLOAT(at+2, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yp1, z_0));
  SETFLOAT(at+3, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, y_0, z_0));
  SETFLOAT(at+4, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yn1, z_0));
  SETFLOAT(at+5, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, y_0, zp1));
  SETFLOAT(at+6, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, y_0, zn1));
  outlet_anything(x->x_obj.ob_outlet, x->x_s_early1, 7, at);

  SETFLOAT(at+1, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp2, y_0, z_0));
  SETFLOAT(at+2, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yp1, z_0));
  SETFLOAT(at+3, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yp2, z_0));
  SETFLOAT(at+4, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yp1, z_0));
  SETFLOAT(at+5, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn2, y_0, z_0));
  SETFLOAT(at+6, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yn1, z_0));
  SETFLOAT(at+7, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yn2, z_0));
  SETFLOAT(at+8, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yn1, z_0));
  SETFLOAT(at+9, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, y_0, zp1));
  SETFLOAT(at+10, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yp1, zp1));
  SETFLOAT(at+11, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, y_0, zp1));
  SETFLOAT(at+12, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yn1, zp1));
  SETFLOAT(at+13, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, y_0, zp2));
  SETFLOAT(at+14, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, y_0, zn1));
  SETFLOAT(at+15, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yp1, zn1));
  SETFLOAT(at+16, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, y_0, zn1));
  SETFLOAT(at+17, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yn1, zn1));
  SETFLOAT(at+18, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, y_0, zn2));
  outlet_anything(x->x_obj.ob_outlet, x->x_s_early2, 19, at);

  SETFLOAT(at+1, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp3, y_0, z_0));
  SETFLOAT(at+2, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp2, yp1, z_0));
  SETFLOAT(at+3, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yp2, z_0));
  SETFLOAT(at+4, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yp3, z_0));
  SETFLOAT(at+5, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yp2, z_0));
  SETFLOAT(at+6, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn2, yp1, z_0));
  SETFLOAT(at+7, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn3, y_0, z_0));
  SETFLOAT(at+8, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn2, yn1, z_0));
  SETFLOAT(at+9, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yn2, z_0));
  SETFLOAT(at+10, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yn3, z_0));
  SETFLOAT(at+11, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yn2, z_0));
  SETFLOAT(at+12, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp2, yn1, z_0));
  SETFLOAT(at+13, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp2, y_0, zp1));
  SETFLOAT(at+14, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yp1, zp1));
  SETFLOAT(at+15, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yp2, zp1));
  SETFLOAT(at+16, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yp1, zp1));
  SETFLOAT(at+17, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn2, y_0, zp1));
  SETFLOAT(at+18, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yn1, zp1));
  SETFLOAT(at+19, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yn2, zp1));
  SETFLOAT(at+20, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yn1, zp1));
  SETFLOAT(at+21, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, y_0, zp2));
  SETFLOAT(at+22, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yp1, zp2));
  SETFLOAT(at+23, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, y_0, zp2));
  SETFLOAT(at+24, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yn1, zp2));
  SETFLOAT(at+25, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, y_0, zp3));
  SETFLOAT(at+26, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp2, y_0, zn1));
  SETFLOAT(at+27, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yp1, zn1));
  SETFLOAT(at+28, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yp2, zn1));
  SETFLOAT(at+29, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yp1, zn1));
  SETFLOAT(at+30, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn2, y_0, zn1));
  SETFLOAT(at+31, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yn1, zn1));
  SETFLOAT(at+32, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yn2, zn1));
  SETFLOAT(at+33, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yn1, zn1));
  SETFLOAT(at+34, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, y_0, zn2));
  SETFLOAT(at+35, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yp1, zn2));
  SETFLOAT(at+36, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, y_0, zn2));
  SETFLOAT(at+37, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yn1, zn2));
  SETFLOAT(at+38, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, y_0, zn3));
  outlet_anything(x->x_obj.ob_outlet, x->x_s_early3, 39, at);

  SETFLOAT(at+1, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp4, y_0, z_0));
  SETFLOAT(at+2, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp3, yp1, z_0));
  SETFLOAT(at+3, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp2, yp2, z_0));
  SETFLOAT(at+4, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yp3, z_0));
  SETFLOAT(at+5, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yp4, z_0));
  SETFLOAT(at+6, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yp3, z_0));
  SETFLOAT(at+7, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn2, yp2, z_0));
  SETFLOAT(at+8, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn3, yp1, z_0));
  SETFLOAT(at+9, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn4, y_0, z_0));
  SETFLOAT(at+10, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn3, yn1, z_0));
  SETFLOAT(at+11, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn2, yn2, z_0));
  SETFLOAT(at+12, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yn3, z_0));
  SETFLOAT(at+13, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yn4, z_0));
  SETFLOAT(at+14, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yn3, z_0));
  SETFLOAT(at+15, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp2, yn2, z_0));
  SETFLOAT(at+16, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp3, yn1, z_0));
  SETFLOAT(at+17, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp3, y_0, zp1));
  SETFLOAT(at+18, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp2, yp1, zp1));
  SETFLOAT(at+19, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yp2, zp1));
  SETFLOAT(at+20, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yp3, zp1));
  SETFLOAT(at+21, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yp2, zp1));
  SETFLOAT(at+22, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn2, yp1, zp1));
  SETFLOAT(at+23, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn3, y_0, zp1));
  SETFLOAT(at+24, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn2, yn1, zp1));
  SETFLOAT(at+25, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yn2, zp1));
  SETFLOAT(at+26, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yn3, zp1));
  SETFLOAT(at+27, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yn2, zp1));
  SETFLOAT(at+28, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp2, yn1, zp1));
  SETFLOAT(at+29, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp2, y_0, zp2));
  SETFLOAT(at+30, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yp1, zp2));
  SETFLOAT(at+31, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yp2, zp2));
  SETFLOAT(at+32, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yp1, zp2));
  SETFLOAT(at+33, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn2, y_0, zp2));
  SETFLOAT(at+34, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yn1, zp2));
  SETFLOAT(at+35, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yn2, zp2));
  SETFLOAT(at+36, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yn1, zp2));
  SETFLOAT(at+37, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, y_0, zp3));
  SETFLOAT(at+38, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yp1, zp3));
  SETFLOAT(at+39, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, y_0, zp3));
  SETFLOAT(at+40, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yn1, zp3));
  SETFLOAT(at+41, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, y_0, zp4));
  SETFLOAT(at+42, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp3, y_0, zn1));
  SETFLOAT(at+43, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp2, yp1, zn1));
  SETFLOAT(at+44, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yp2, zn1));
  SETFLOAT(at+45, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yp3, zn1));
  SETFLOAT(at+46, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yp2, zn1));
  SETFLOAT(at+47, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn2, yp1, zn1));
  SETFLOAT(at+48, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn3, y_0, zn1));
  SETFLOAT(at+49, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn2, yn1, zn1));
  SETFLOAT(at+50, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yn2, zn1));
  SETFLOAT(at+51, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yn3, zn1));
  SETFLOAT(at+52, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yn2, zn1));
  SETFLOAT(at+53, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp2, yn1, zn1));
  SETFLOAT(at+54, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp2, y_0, zn2));
  SETFLOAT(at+55, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yp1, zn2));
  SETFLOAT(at+56, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yp2, zn2));
  SETFLOAT(at+57, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yp1, zn2));
  SETFLOAT(at+58, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn2, y_0, zn2));
  SETFLOAT(at+59, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, yn1, zn2));
  SETFLOAT(at+60, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yn2, zn2));
  SETFLOAT(at+61, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, yn1, zn2));
  SETFLOAT(at+62, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xp1, y_0, zn3));
  SETFLOAT(at+63, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yp1, zn3));
  SETFLOAT(at+64, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, xn1, y_0, zn3));
  SETFLOAT(at+65, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, yn1, zn3));
  SETFLOAT(at+66, xyz_3_del_damp_theta_phi_calc_elevation(x_180_over_pi, x_0, y_0, zn4));
  outlet_anything(x->x_obj.ob_outlet, x->x_s_early4, 67, at);
 


  SETSYMBOL(at, x->x_s_phi);
  
  SETFLOAT(at+1, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, y_0, z_0));
  outlet_anything(x->x_obj.ob_outlet, x->x_s_direct, 2, at);
  
  SETFLOAT(at+1, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, y_0, z_0));
  SETFLOAT(at+2, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yp1, z_0));
  SETFLOAT(at+3, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, y_0, z_0));
  SETFLOAT(at+4, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yn1, z_0));
  SETFLOAT(at+5, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, y_0, zp1));
  SETFLOAT(at+6, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, y_0, zn1));
  outlet_anything(x->x_obj.ob_outlet, x->x_s_early1, 7, at);

  SETFLOAT(at+1, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp2, y_0, z_0));
  SETFLOAT(at+2, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yp1, z_0));
  SETFLOAT(at+3, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yp2, z_0));
  SETFLOAT(at+4, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yp1, z_0));
  SETFLOAT(at+5, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn2, y_0, z_0));
  SETFLOAT(at+6, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yn1, z_0));
  SETFLOAT(at+7, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yn2, z_0));
  SETFLOAT(at+8, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yn1, z_0));
  SETFLOAT(at+9, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, y_0, zp1));
  SETFLOAT(at+10, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yp1, zp1));
  SETFLOAT(at+11, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, y_0, zp1));
  SETFLOAT(at+12, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yn1, zp1));
  SETFLOAT(at+13, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, y_0, zp2));
  SETFLOAT(at+14, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, y_0, zn1));
  SETFLOAT(at+15, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yp1, zn1));
  SETFLOAT(at+16, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, y_0, zn1));
  SETFLOAT(at+17, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yn1, zn1));
  SETFLOAT(at+18, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, y_0, zn2));
  outlet_anything(x->x_obj.ob_outlet, x->x_s_early2, 19, at);

  SETFLOAT(at+1, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp3, y_0, z_0));
  SETFLOAT(at+2, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp2, yp1, z_0));
  SETFLOAT(at+3, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yp2, z_0));
  SETFLOAT(at+4, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yp3, z_0));
  SETFLOAT(at+5, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yp2, z_0));
  SETFLOAT(at+6, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn2, yp1, z_0));
  SETFLOAT(at+7, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn3, y_0, z_0));
  SETFLOAT(at+8, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn2, yn1, z_0));
  SETFLOAT(at+9, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yn2, z_0));
  SETFLOAT(at+10, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yn3, z_0));
  SETFLOAT(at+11, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yn2, z_0));
  SETFLOAT(at+12, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp2, yn1, z_0));
  SETFLOAT(at+13, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp2, y_0, zp1));
  SETFLOAT(at+14, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yp1, zp1));
  SETFLOAT(at+15, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yp2, zp1));
  SETFLOAT(at+16, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yp1, zp1));
  SETFLOAT(at+17, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn2, y_0, zp1));
  SETFLOAT(at+18, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yn1, zp1));
  SETFLOAT(at+19, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yn2, zp1));
  SETFLOAT(at+20, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yn1, zp1));
  SETFLOAT(at+21, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, y_0, zp2));
  SETFLOAT(at+22, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yp1, zp2));
  SETFLOAT(at+23, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, y_0, zp2));
  SETFLOAT(at+24, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yn1, zp2));
  SETFLOAT(at+25, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, y_0, zp3));
  SETFLOAT(at+26, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp2, y_0, zn1));
  SETFLOAT(at+27, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yp1, zn1));
  SETFLOAT(at+28, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yp2, zn1));
  SETFLOAT(at+29, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yp1, zn1));
  SETFLOAT(at+30, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn2, y_0, zn1));
  SETFLOAT(at+31, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yn1, zn1));
  SETFLOAT(at+32, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yn2, zn1));
  SETFLOAT(at+33, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yn1, zn1));
  SETFLOAT(at+34, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, y_0, zn2));
  SETFLOAT(at+35, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yp1, zn2));
  SETFLOAT(at+36, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, y_0, zn2));
  SETFLOAT(at+37, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yn1, zn2));
  SETFLOAT(at+38, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, y_0, zn3));
  outlet_anything(x->x_obj.ob_outlet, x->x_s_early3, 39, at);

  SETFLOAT(at+1, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp4, y_0, z_0));
  SETFLOAT(at+2, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp3, yp1, z_0));
  SETFLOAT(at+3, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp2, yp2, z_0));
  SETFLOAT(at+4, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yp3, z_0));
  SETFLOAT(at+5, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yp4, z_0));
  SETFLOAT(at+6, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yp3, z_0));
  SETFLOAT(at+7, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn2, yp2, z_0));
  SETFLOAT(at+8, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn3, yp1, z_0));
  SETFLOAT(at+9, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn4, y_0, z_0));
  SETFLOAT(at+10, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn3, yn1, z_0));
  SETFLOAT(at+11, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn2, yn2, z_0));
  SETFLOAT(at+12, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yn3, z_0));
  SETFLOAT(at+13, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yn4, z_0));
  SETFLOAT(at+14, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yn3, z_0));
  SETFLOAT(at+15, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp2, yn2, z_0));
  SETFLOAT(at+16, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp3, yn1, z_0));
  SETFLOAT(at+17, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp3, y_0, zp1));
  SETFLOAT(at+18, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp2, yp1, zp1));
  SETFLOAT(at+19, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yp2, zp1));
  SETFLOAT(at+20, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yp3, zp1));
  SETFLOAT(at+21, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yp2, zp1));
  SETFLOAT(at+22, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn2, yp1, zp1));
  SETFLOAT(at+23, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn3, y_0, zp1));
  SETFLOAT(at+24, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn2, yn1, zp1));
  SETFLOAT(at+25, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yn2, zp1));
  SETFLOAT(at+26, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yn3, zp1));
  SETFLOAT(at+27, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yn2, zp1));
  SETFLOAT(at+28, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp2, yn1, zp1));
  SETFLOAT(at+29, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp2, y_0, zp2));
  SETFLOAT(at+30, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yp1, zp2));
  SETFLOAT(at+31, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yp2, zp2));
  SETFLOAT(at+32, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yp1, zp2));
  SETFLOAT(at+33, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn2, y_0, zp2));
  SETFLOAT(at+34, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yn1, zp2));
  SETFLOAT(at+35, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yn2, zp2));
  SETFLOAT(at+36, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yn1, zp2));
  SETFLOAT(at+37, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, y_0, zp3));
  SETFLOAT(at+38, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yp1, zp3));
  SETFLOAT(at+39, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, y_0, zp3));
  SETFLOAT(at+40, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yn1, zp3));
  SETFLOAT(at+41, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, y_0, zp4));
  SETFLOAT(at+42, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp3, y_0, zn1));
  SETFLOAT(at+43, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp2, yp1, zn1));
  SETFLOAT(at+44, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yp2, zn1));
  SETFLOAT(at+45, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yp3, zn1));
  SETFLOAT(at+46, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yp2, zn1));
  SETFLOAT(at+47, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn2, yp1, zn1));
  SETFLOAT(at+48, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn3, y_0, zn1));
  SETFLOAT(at+49, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn2, yn1, zn1));
  SETFLOAT(at+50, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yn2, zn1));
  SETFLOAT(at+51, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yn3, zn1));
  SETFLOAT(at+52, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yn2, zn1));
  SETFLOAT(at+53, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp2, yn1, zn1));
  SETFLOAT(at+54, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp2, y_0, zn2));
  SETFLOAT(at+55, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yp1, zn2));
  SETFLOAT(at+56, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yp2, zn2));
  SETFLOAT(at+57, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yp1, zn2));
  SETFLOAT(at+58, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn2, y_0, zn2));
  SETFLOAT(at+59, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, yn1, zn2));
  SETFLOAT(at+60, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yn2, zn2));
  SETFLOAT(at+61, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, yn1, zn2));
  SETFLOAT(at+62, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xp1, y_0, zn3));
  SETFLOAT(at+63, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yp1, zn3));
  SETFLOAT(at+64, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, xn1, y_0, zn3));
  SETFLOAT(at+65, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, yn1, zn3));
  SETFLOAT(at+66, xyz_3_del_damp_theta_phi_calc_azimuth(x_180_over_pi, x_0, y_0, zn4));
  outlet_anything(x->x_obj.ob_outlet, x->x_s_early4, 67, at);
}

static void xyz_3_del_damp_theta_phi_src_xyz(t_xyz_3_del_damp_theta_phi *x, t_symbol *s, int argc, t_atom *argv)
{
  if((argc >= 3)&&IS_A_FLOAT(argv, 0)&&IS_A_FLOAT(argv, 1)&&IS_A_FLOAT(argv, 2))
  {
    t_float xr2=0.5f*x->x_room_x, yr2=0.5f*x->x_room_y;
    
    x->x_src_x = atom_getfloat(argv++);
    x->x_src_y = atom_getfloat(argv++);
    x->x_src_z = atom_getfloat(argv);
    if(x->x_src_x > xr2)
      x->x_src_x = xr2;
    if(x->x_src_x < -xr2)
      x->x_src_x = -xr2;
    if(x->x_src_y > yr2)
      x->x_src_y = yr2;
    if(x->x_src_y < -yr2)
      x->x_src_y = -yr2;
    if(x->x_src_z > x->x_room_z)
      x->x_src_z = x->x_room_z;
    if(x->x_src_z < 0.0f)
      x->x_src_z = 0.0f;
    clock_delay(x->x_clock, 0.0f);
  }
}

static void xyz_3_del_damp_theta_phi_head_xyz(t_xyz_3_del_damp_theta_phi *x, t_symbol *s, int argc, t_atom *argv)
{
  if((argc >= 3)&&IS_A_FLOAT(argv, 0)&&IS_A_FLOAT(argv, 1)&&IS_A_FLOAT(argv, 2))
  {
    t_float xr2=0.5f*x->x_room_x, yr2=0.5f*x->x_room_y;
    
    x->x_head_x = atom_getfloat(argv++);
    x->x_head_y = atom_getfloat(argv++);
    x->x_head_z = atom_getfloat(argv);
    if(x->x_head_x > xr2)
      x->x_head_x = xr2;
    if(x->x_head_x < -xr2)
      x->x_head_x = -xr2;
    if(x->x_head_y > yr2)
      x->x_head_y = yr2;
    if(x->x_head_y < -yr2)
      x->x_head_y = -yr2;
    if(x->x_head_z > x->x_room_z)
      x->x_head_z = x->x_room_z;
    if(x->x_head_z < 0.0f)
      x->x_head_z = 0.0f;
    clock_delay(x->x_clock, 0.0f);
  }
}

static void xyz_3_del_damp_theta_phi_room_xyz(t_xyz_3_del_damp_theta_phi *x, t_symbol *s, int argc, t_atom *argv)
{
  if((argc >= 3)&&IS_A_FLOAT(argv, 0)&&IS_A_FLOAT(argv, 1)&&IS_A_FLOAT(argv, 2))
  {
    t_float xr2=0.0f, yr2=0.0f;

    x->x_room_x = atom_getfloat(argv++);
    x->x_room_y = atom_getfloat(argv++);
    x->x_room_z = atom_getfloat(argv);
    if(x->x_room_x < 0.5f)
      x->x_room_x = 0.5f;
    if(x->x_room_y < 0.5f)
      x->x_room_y = 0.5f;
    if(x->x_room_z < 0.5f)
      x->x_room_z = 0.5f;

    xr2 = 0.5f*x->x_room_x;
    yr2 = 0.5f*x->x_room_y;

    if(x->x_src_x > xr2)
      x->x_src_x = xr2;
    if(x->x_src_x < -xr2)
      x->x_src_x = -xr2;
    if(x->x_src_y > yr2)
      x->x_src_y = yr2;
    if(x->x_src_y < -yr2)
      x->x_src_y = -yr2;
    if(x->x_src_z > x->x_room_z)
      x->x_src_z = x->x_room_z;
    if(x->x_src_z < 0.0f)
      x->x_src_z = 0.0f;

    if(x->x_head_x > xr2)
      x->x_head_x = xr2;
    if(x->x_head_x < -xr2)
      x->x_head_x = -xr2;
    if(x->x_head_y > yr2)
      x->x_head_y = yr2;
    if(x->x_head_y < -yr2)
      x->x_head_y = -yr2;
    if(x->x_head_z > x->x_room_z)
      x->x_head_z = x->x_room_z;
    if(x->x_head_z < 0.0f)
      x->x_head_z = 0.0f;

    clock_delay(x->x_clock, 0.0f);
  }
}

static void xyz_3_del_damp_theta_phi_r_min(t_xyz_3_del_damp_theta_phi *x, t_float r_min)
{
  if(r_min < 0.1f)
    r_min = 0.1f;
  x->x_r_min = r_min;
  clock_delay(x->x_clock, 0.0f);
}

static void xyz_3_del_damp_theta_phi_sonic_speed(t_xyz_3_del_damp_theta_phi *x, t_float speed)
{
  if(speed < 10.0f)
    speed = 10.0f;
  if(speed > 2000.0f)
    speed = 2000.0f;
  x->x_speed = speed;
  clock_delay(x->x_clock, 0.0f);
}

static void xyz_3_del_damp_theta_phi_free(t_xyz_3_del_damp_theta_phi *x)
{
  clock_free(x->x_clock);
  
	freebytes(x->x_at, x->x_size_at * sizeof(t_atom));
  freebytes(x->x_rad, x->x_size_rad * sizeof(t_float));
}

static void *xyz_3_del_damp_theta_phi_new(t_symbol *s, int argc, t_atom *argv)
{
  t_xyz_3_del_damp_theta_phi *x = (t_xyz_3_del_damp_theta_phi *)pd_new(xyz_3_del_damp_theta_phi_class);
  
  x->x_room_x = 12.0f;
  x->x_room_y = 8.0f;
  x->x_room_z = 4.0f;
  x->x_head_x = 0.0f;
  x->x_head_y = 0.0f;
  x->x_head_z = 1.7f;
  x->x_src_x = 3.0f;
  x->x_src_y = 0.5f;
  x->x_src_z = 2.5f;
  x->x_r_min = 1.4f;
  x->x_speed = 340.0f;
  x->x_s_direct = gensym("direct");
  x->x_s_early1 = gensym("early1");
  x->x_s_early2 = gensym("early2");
  x->x_s_early3 = gensym("early3");
  x->x_s_early4 = gensym("early4");
  x->x_s_damp = gensym("damp");
  x->x_s_del = gensym("del");
  x->x_s_theta = gensym("theta");
  x->x_s_phi = gensym("phi");
  x->x_size_rad = 129; // 1 + 6 + 18 + 38 + 66; 129=1+(1+4)+(4+9)+(9+16)+(16+25)+(9+16)+(4+9)+(1+4)+1
  x->x_size_at = 67; // 66 + 1; 66=1+4+8+12+16+12+8+4+1; 38=1+4+8+12+8+4+1
  x->x_rad = (t_float *)getbytes(x->x_size_rad * sizeof(t_float));
  x->x_at = (t_atom *)getbytes(x->x_size_at * sizeof(t_atom));
  outlet_new(&x->x_obj, &s_list);
  x->x_clock = clock_new(x, (t_method)xyz_3_del_damp_theta_phi_doit);
  x->x_180_over_pi  = (t_float)(180.0 / (4.0 * atan(1.0)));
  return (x);
}

void xyz_3_del_damp_theta_phi_setup(void)
{
  xyz_3_del_damp_theta_phi_class = class_new(gensym("xyz_3_del_damp_theta_phi"), (t_newmethod)xyz_3_del_damp_theta_phi_new, (t_method)xyz_3_del_damp_theta_phi_free,
    sizeof(t_xyz_3_del_damp_theta_phi), 0, A_GIMME, 0);
  class_addmethod(xyz_3_del_damp_theta_phi_class, (t_method)xyz_3_del_damp_theta_phi_src_xyz, gensym("src_xyz"), A_GIMME, 0);
  class_addmethod(xyz_3_del_damp_theta_phi_class, (t_method)xyz_3_del_damp_theta_phi_head_xyz, gensym("head_xyz"), A_GIMME, 0);
  class_addmethod(xyz_3_del_damp_theta_phi_class, (t_method)xyz_3_del_damp_theta_phi_room_xyz, gensym("room_xyz"), A_GIMME, 0);
  class_addmethod(xyz_3_del_damp_theta_phi_class, (t_method)xyz_3_del_damp_theta_phi_sonic_speed, gensym("sonic_speed"), A_FLOAT, 0);
  class_addmethod(xyz_3_del_damp_theta_phi_class, (t_method)xyz_3_del_damp_theta_phi_r_min, gensym("r_min"), A_FLOAT, 0);
}
