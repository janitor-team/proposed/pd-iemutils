/* For information on usage and redistribution, and for a DISCLAIMER OF ALL
* WARRANTIES, see the file, "LICENSE.txt," in this distribution.

iem_roomsim written by Thomas Musil (c) IEM KUG Graz Austria 2002 - 2018 */

/* no good, hat schwirigkeiten bei help patch, kontinuierlich die formanten interpolation zu schaffen */


#include "m_pd.h"
#include "iemlib.h"
#include "iem_roomsim.h"

/* -------------------------- n_delay1p_line_tilde~ ------------------------------ */
/* ----------- n non interpolated line~-signal driven delay taps ------------- */

static t_class *n_delay1p_line_tilde_class;

typedef struct _n_delay1p_line_tilde
{
	t_object	x_obj;
	int				x_mallocsize;
	t_float		x_max_delay_ms;
	t_sample	*x_begmem1;
	t_sample	*x_begmem2;
	int				x_writeindex;
	int				x_n_delays;
	int				*x_del_samp256_end;
	int				*x_del_samp256_cur;
	int				*x_inc256;
	int				*x_biginc256;
	int				x_blocksize;
	t_float		x_sr;
	t_float		x_ms2tick;
	t_float		x_ms2samples256;
	t_float		x_interpol_ms;
	int				x_interpol_ticks;
	int				x_ticksleft;
	int				x_old;
	int				x_retarget;
	t_sample	**x_io;
	t_float		x_scalar_sig_in;
} t_n_delay1p_line_tilde;

static void n_delay1p_line_tilde_list(t_n_delay1p_line_tilde *x, t_symbol *s, int argc, t_atom *argv)
{
	if(argc == x->x_n_delays)
	{
		int i;
		t_float delay, max=x->x_max_delay_ms;

		if(x->x_interpol_ms <= (t_float)0.0)
			x->x_ticksleft = x->x_retarget = 0;
		else
			x->x_retarget = 1;
		for(i=0; i<argc; i++)
		{
			delay = atom_getfloat(argv++);
			if(delay < (t_float)0.0)
				delay = (t_float)0.0;
			if(delay > max)
				delay = max;
			if(x->x_interpol_ms <= (t_float)0.0)
				x->x_del_samp256_end[i] = x->x_del_samp256_cur[i] = (int)(x->x_ms2samples256 * delay + (t_float)0.5) + 127;
			else
				x->x_del_samp256_end[i] = (int)(x->x_ms2samples256 * delay  + (t_float)0.5) + 127;
		}
	}
}

static void n_delay1p_line_tilde_time(t_n_delay1p_line_tilde *x, t_floatarg interpol_ms)
{
	if(interpol_ms < (t_float)0.0)
		interpol_ms = (t_float)0.0;
	x->x_interpol_ms = interpol_ms;
	x->x_interpol_ticks = (int)(x->x_ms2tick * interpol_ms);
}

static void n_delay1p_line_tilde_stop(t_n_delay1p_line_tilde *x)
{
	int i, n=x->x_n_delays;

	for(i=0; i<n; i++)
		x->x_del_samp256_end[i] = x->x_del_samp256_cur[i];
	x->x_ticksleft = x->x_retarget = 0;
}

static t_int *n_delay1p_line_tilde_perform(t_int *w)
{
	t_n_delay1p_line_tilde *x = (t_n_delay1p_line_tilde *)(w[1]);
	int hn=(int)(w[2]);
	int nout=x->x_n_delays;
	t_sample *in;
	t_sample *out;
	int writeindex = x->x_writeindex;
	int i, j, n;
	int malloc_samples = x->x_mallocsize;
	t_sample *begvec1 = x->x_begmem1;
	t_sample *begvec2 = x->x_begmem2;
	t_sample *writevec;
	t_sample *readvec;
	int del256, inc256;

	begvec1 += writeindex;
  writevec = begvec2 + writeindex;
	in=x->x_io[0];
	n = hn;
	while(n--)
	{
		*begvec1++ = *in;
    *writevec++ = *in++;
	}

	if(x->x_retarget)
	{
		int nticks = x->x_interpol_ticks;

		if(!nticks)
			nticks = 1;
		x->x_ticksleft = nticks;
		for(j=0; j<nout; j++)
		{
			x->x_biginc256[j] = (x->x_del_samp256_end[j] - x->x_del_samp256_cur[j]) / nticks;
			x->x_inc256[j] = x->x_biginc256[j] / x->x_blocksize;
		}
		x->x_retarget = 0;
	}

	if(x->x_ticksleft)
	{
		for(j=0; j<nout; j++)
		{
			inc256 = x->x_inc256[j];
			del256 = x->x_del_samp256_cur[j];
			out = x->x_io[j+1];
			for(i=0; i<hn; i++)
			{
				readvec = begvec2 + writeindex - (del256 >> 8) + i;
				*out++ = *readvec;
				del256 += inc256;
			}
			x->x_del_samp256_cur[j] += x->x_biginc256[j];
		}
		x->x_ticksleft--;
	}
	else
	{
		for(j=0; j<nout; j++)
		{
			del256 = x->x_del_samp256_cur[j] = x->x_del_samp256_end[j];
			readvec = begvec2 + writeindex - (del256 >> 8);
			out = x->x_io[j+1];
			n = hn;
			while(n--)
			{
				*out++ = *readvec++;
			}
		}
	}
	writeindex += hn;
	if(writeindex >= malloc_samples)
	{
		writeindex -= malloc_samples;
	}
	x->x_writeindex = writeindex;
	return(w+3);
}

static t_int *n_delay1p_line_tilde_perf8(t_int *w)
{
	t_n_delay1p_line_tilde *x = (t_n_delay1p_line_tilde *)(w[1]);
	int hn=(int)(w[2]);
	int nout=x->x_n_delays;
	t_sample *in;
	t_sample *out;
	int writeindex = x->x_writeindex;
	int i, j, k, n;
	int malloc_samples = x->x_mallocsize;
	t_sample *begvec1 = x->x_begmem1;
	t_sample *begvec2 = x->x_begmem2;
	t_sample *writevec;
	t_sample *readvec;
	int del256, inc256;

//	post("writevec = %d",writeindex);
	begvec1 += writeindex;
  writevec = begvec2 + writeindex;
	in=x->x_io[0];
	n = hn;
	while(n)
	{
		begvec1[0] = in[0];
		begvec1[1] = in[1];
		begvec1[2] = in[2];
		begvec1[3] = in[3];
		begvec1[4] = in[4];
		begvec1[5] = in[5];
		begvec1[6] = in[6];
		begvec1[7] = in[7];
    
    writevec[0] = in[0];
		writevec[1] = in[1];
		writevec[2] = in[2];
		writevec[3] = in[3];
		writevec[4] = in[4];
		writevec[5] = in[5];
		writevec[6] = in[6];
		writevec[7] = in[7];

    begvec1 += 8;
		writevec += 8;
		n -= 8;
		in += 8;
	}

	if(x->x_retarget)
	{
		int nticks = x->x_interpol_ticks;

		if(!nticks)
			nticks = 1;
		x->x_ticksleft = nticks;
		for(j=0; j<nout; j++)
		{
			x->x_biginc256[j] = (x->x_del_samp256_end[j] - x->x_del_samp256_cur[j]) / nticks;
			x->x_inc256[j] = x->x_biginc256[j] / x->x_blocksize;
		}
		x->x_retarget = 0;
	}

	if(x->x_ticksleft)
	{
		for(j=0; j<nout; j++)
		{
			inc256 = x->x_inc256[j];
			del256 = x->x_del_samp256_cur[j];
			out = x->x_io[j+1];
			readvec = begvec2 + writeindex;
			for(i=0; i<hn; i+=8)
			{
				k = del256 >> 8;
				out[0] = readvec[0-k];
				del256 += inc256;

				k = del256 >> 8;
				out[1] = readvec[1-k];
				del256 += inc256;

				k = del256 >> 8;
				out[2] = readvec[2-k];
				del256 += inc256;

				k = del256 >> 8;
				out[3] = readvec[3-k];
				del256 += inc256;

				k = del256 >> 8;
				out[4] = readvec[4-k];
				del256 += inc256;

				k = del256 >> 8;
				out[5] = readvec[5-k];
				del256 += inc256;

				k = del256 >> 8;
				out[6] = readvec[6-k];
				del256 += inc256;

				k = del256 >> 8;
				out[7] = readvec[7-k];
				del256 += inc256;

				out += 8;
				readvec += 8;
			}
			x->x_del_samp256_cur[j] += x->x_biginc256[j];
		}
		x->x_ticksleft--;
	}
	else
	{
		for(j=0; j<nout; j++)
		{
			del256 = x->x_del_samp256_cur[j] = x->x_del_samp256_end[j];
			readvec = begvec2 + writeindex - (del256 >> 8);
			out = x->x_io[j+1];
			n = hn;
			while(n)
			{
				out[0] = readvec[0];
				out[1] = readvec[1];
				out[2] = readvec[2];
				out[3] = readvec[3];
				out[4] = readvec[4];
				out[5] = readvec[5];
				out[6] = readvec[6];
				out[7] = readvec[7];
				out += 8;
				readvec += 8;
				n -= 8;
			}
		}
	}

	writeindex += hn;
	if(writeindex >= malloc_samples)
	{
		writeindex -= malloc_samples;
	}
	x->x_writeindex = writeindex;

	return(w+3);
}

static void n_delay1p_line_tilde_dsp(t_n_delay1p_line_tilde *x, t_signal **sp)
{
	int n = sp[0]->s_n;
	int i, nd = x->x_n_delays + 1;

	if(!x->x_blocksize)/*first time*/
	{
		int nsamps = x->x_max_delay_ms * (t_float)sp[0]->s_sr * (t_float)0.001;

		if(nsamps < 1)
			nsamps = 1;
		nsamps += ((- nsamps) & (n - 1));
		nsamps += n;
		x->x_mallocsize = nsamps;
		x->x_begmem1 = (t_sample *)getbytes(2 * x->x_mallocsize * sizeof(t_sample));
		x->x_begmem2 = x->x_begmem1 + x->x_mallocsize;
		x->x_writeindex = n;
	}
	else if((x->x_blocksize != n) || ((t_float)sp[0]->s_sr != x->x_sr))
	{
		int nsamps = x->x_max_delay_ms * (t_float)sp[0]->s_sr * (t_float)0.001;

		if(nsamps < 1)
			nsamps = 1;
		nsamps += ((- nsamps) & (n - 1));
		nsamps += n;
		
		x->x_begmem1 = (t_sample *)resizebytes(x->x_begmem1, 2*x->x_mallocsize*sizeof(t_sample), 2*nsamps*sizeof(t_sample));
		x->x_mallocsize = nsamps;
		x->x_begmem2 = x->x_begmem1 + x->x_mallocsize;
		if(x->x_writeindex >= nsamps)
			x->x_writeindex -= nsamps;
	}
	x->x_blocksize = n;
	x->x_ms2tick = (t_float)0.001 * (t_float)sp[0]->s_sr / (t_float)n;
	x->x_ms2samples256 = (t_float)0.256 * (t_float)sp[0]->s_sr;
	x->x_interpol_ticks = (int)(x->x_ms2tick * x->x_interpol_ms);
	for(i=0; i<nd; i++)
		x->x_io[i] = sp[i]->s_vec;
	if(n&7)
		dsp_add(n_delay1p_line_tilde_perform, 2, x, n);
	else
		dsp_add(n_delay1p_line_tilde_perf8, 2, x, n);
}

static void *n_delay1p_line_tilde_new(t_floatarg fout, t_floatarg delay_ms, t_floatarg interpol_ms)
{
	t_n_delay1p_line_tilde *x = (t_n_delay1p_line_tilde *)pd_new(n_delay1p_line_tilde_class);
	int i, n_out = (int)fout;
	int nsamps = delay_ms * sys_getsr() * (t_float)0.001;

	if(n_out < 1)
		n_out = 1;
	x->x_n_delays = n_out;
	x->x_max_delay_ms = delay_ms;
	if(nsamps < 1)
		nsamps = 1;
	nsamps += ((- nsamps) & (DELLINE_DEF_VEC_SIZE - 1));
	nsamps += DELLINE_DEF_VEC_SIZE;
	x->x_mallocsize = nsamps;
	x->x_begmem1 = (t_sample *)getbytes(2 * x->x_mallocsize * sizeof(t_sample));
	x->x_begmem2 = x->x_begmem1 + x->x_mallocsize;
	x->x_writeindex = DELLINE_DEF_VEC_SIZE;
	x->x_blocksize = 0;
	x->x_sr = (t_float)0.0;
	if(interpol_ms < (t_float)0.0)
		interpol_ms = (t_float)0.0;
	x->x_interpol_ms = interpol_ms;
	x->x_io = (t_sample **)getbytes((x->x_n_delays + 1) * sizeof(t_sample *));
	for(i=0; i<n_out; i++)
		outlet_new(&x->x_obj, &s_signal);
	x->x_del_samp256_end = (int *)getbytes(x->x_n_delays * sizeof(int));
	x->x_del_samp256_cur = (int *)getbytes(x->x_n_delays * sizeof(int));
	x->x_inc256 = (int *)getbytes(x->x_n_delays * sizeof(int));
	x->x_biginc256 = (int *)getbytes(x->x_n_delays * sizeof(int));
	x->x_ticksleft = x->x_retarget = 0;
	for(i=0; i<n_out; i++)
	{
		x->x_del_samp256_cur[i] = x->x_del_samp256_end[i] = 0;
		x->x_inc256[i] = x->x_biginc256[i] = 0;
	}
	x->x_interpol_ticks = 0;
	x->x_scalar_sig_in = (t_float)0.0;
	return (x);
}

static void n_delay1p_line_tilde_free(t_n_delay1p_line_tilde *x)
{
	freebytes(x->x_del_samp256_end, x->x_n_delays * sizeof(int));
	freebytes(x->x_del_samp256_cur, x->x_n_delays * sizeof(int));
	freebytes(x->x_inc256, x->x_n_delays * sizeof(int));
	freebytes(x->x_biginc256, x->x_n_delays * sizeof(int));
	freebytes(x->x_io, (x->x_n_delays + 1) * sizeof(t_sample *));
	freebytes(x->x_begmem1, 2 * x->x_mallocsize * sizeof(t_sample));
}

void n_delay1p_line_tilde_setup(void)
{
	n_delay1p_line_tilde_class = class_new(gensym("n_delay1p_line~"), (t_newmethod)n_delay1p_line_tilde_new, (t_method)n_delay1p_line_tilde_free,
		sizeof(t_n_delay1p_line_tilde), 0, A_DEFFLOAT, A_DEFFLOAT, A_DEFFLOAT, 0);
	CLASS_MAINSIGNALIN(n_delay1p_line_tilde_class, t_n_delay1p_line_tilde, x_scalar_sig_in);
	class_addlist(n_delay1p_line_tilde_class, (t_method)n_delay1p_line_tilde_list);
	class_addmethod(n_delay1p_line_tilde_class, (t_method)n_delay1p_line_tilde_dsp, gensym("dsp"), A_CANT, 0);
	class_addmethod(n_delay1p_line_tilde_class, (t_method)n_delay1p_line_tilde_stop, gensym("stop"), 0);
	class_addmethod(n_delay1p_line_tilde_class, (t_method)n_delay1p_line_tilde_time, gensym("time"), A_FLOAT, 0);
}
