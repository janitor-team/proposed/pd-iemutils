# META helper plugin for triggerize-selection
# META DESCRIPTION adds menu to tell the 'triggerize' library to doit
# META AUTHOR IOhannes m zm�lnig <zmoelnig@umlaeute.mur.at>
# META VERSION 0.1

package require pdwindow 0.1
if [catch {
    package require msgcat
    ::msgcat::mcload po
}] { puts "iem::punish::triggerize: i18n failed" }

namespace eval ::iem::punish::triggerize:: {
    variable label
    proc focus {winid state} {
        set menustate [expr $state?"normal":"disabled"]
        .menubar.edit entryconfigure "$::iem::punish::triggerize::label" -state $menustate
    }
    proc register {} {
        # create an entry for our "triggerize" in the "edit" menu
        set ::iem::punish::triggerize::label [_ "Triggerize Selection"]
        set accelerator $::pd_menus::accelerator
        set mymenu .menubar.edit
        if {$::windowingsystem eq "aqua"} {
            set inserthere 8
            set accelerator "$accelerator"
        } else {
            set inserthere 8
            set accelerator "$accelerator"
        }
        set accelerator "$accelerator+T"

        $mymenu insert $inserthere command \
            -label $::iem::punish::triggerize::label \
            -state disabled \
            -accelerator "$accelerator" \
            -command { menu_send $::focused_window triggerize }

        bind all <$::modifier-Key-t> {menu_send %W triggerize}
        bind PatchWindow <FocusIn> "+::iem::punish::triggerize::focus %W 1"
        bind PdWindow    <FocusIn> "+::iem::punish::triggerize::focus %W 0"

        # attempt to load the 'triggerize' library from iem::punish
        # (that does all the work)
        set lib [string map {" " "\\ "} [file join $::current_plugin_loadpath triggerize]]
        pdsend "pd-_float_template declare -lib $lib"

        ::pdwindow::post "loaded iem::punish::triggerize-plugin\n"
    }
}

if {[ expr $::PD_MAJOR_VERSION.$::PD_MINOR_VERSION < 0.49 ]} {
    ::iem::punish::triggerize::register
} {
    ::pdwindow::debug "disabled iem::punish::triggerize on Pd>=0.49\n"
}
