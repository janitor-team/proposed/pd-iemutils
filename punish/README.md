punish - IEM's Pure Data User Interface Hacks
=============================================

a collection of hacks for the Pd User Interface

# BIG FAT WARNING

This collection is intended to explore ideas on how to improve Pd's user
interface (from a library perspective).

There are known crasher bugs.

# Contents

## [patcherize](patcherize-plugin)
turn selected objects into a subpatch (or abstraction)

## [triggerize](triggerize-plugin)
- replace fan-outs with [trigger]s
- expand [trigger] to the *left*
- insert [trigger] into wires

## [double-chord](doublechord-plugin)
- duplicate connections between two multi-iolet objects

# LICENSE

each hack comes with its own license

# AUTHORS

- IOhannes m zmölnig
