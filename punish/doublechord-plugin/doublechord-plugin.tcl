# META helper plugin for doublechord-selection
# META DESCRIPTION adds menu to tell the 'doublechord' library to doit
# META AUTHOR IOhannes m zm�lnig <zmoelnig@umlaeute.mur.at>
# META VERSION 0.1

package require pdwindow 0.1
namespace eval ::iem::punish::doublechord:: {
    proc register {} {
        # attempt to load the 'doublechord' library from iem::punish
        # (that does all the work)
        set lib [string map {" " "\\ "} [file join $::current_plugin_loadpath doublechord]]
        pdsend "pd-_float_template declare -lib $lib"

        ::pdwindow::post "loaded iem::punish::doublechord-plugin\n"
    }
}

if {[ expr $::PD_MAJOR_VERSION.$::PD_MINOR_VERSION < 0.49 ]} {
    ::iem::punish::doublechord::register
} {
    ::pdwindow::debug "disabled iem::punish::doublechord on Pd>=0.49\n"
}
