Developer information for iem_utils
===================================

"iem_utils" is META-library that aggragates a number of small (dependency-free)
libraries and GUI-plugins for Pd.

All libraries are developped separately, and are included here by means of
git-submodules.


## Checkout

~~~sh
git clone https://git.iem.at/pd/iem_utils
cd iem_utils
git submodule update --init
~~~

## Building

There is a master Makefile that recursively builds all the libraries (that need
building)

~~~sh
make
~~~

## Updating

git-submodules are set to a specific commit (each) - rather than tracking a
remote ("live") branch.
The following brings all submodules up-to-date with their current 'master'
branches.

~~~sh
git submodule foreach git checkout master
git submodule foreach git pull
~~~

## Pushing changes in submodules

TODO 
